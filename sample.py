# coding: utf-8
import sys
import os
import time

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import svm
from sklearn.metrics import classification_report
from sklearn.feature_extraction import DictVectorizer
from sklearn.cross_validation import train_test_split
from numpy import array

class Predictor:
    def __init__(self):
        print "Predictor running ..."

    def predict(self):

        data_dir = "./text"

        classes = ['KRW', 'KPD', 'NIP', 'NMP', 'NMV', 'UPS']

        text_file = open("txt/sentences.txt", "r")
        lines = text_file.read().split('.')

        tags_text_file = open("txt/tags.txt", "r")
        tag_lines = tags_text_file.read().split('.')
       	
        tagged_list = []

        t_lables = []

        for idx, val in enumerate(lines):
        	sentences = val.split(' ')
        	for idx1, val in enumerate(sentences):
        		# print val + tag_lines[idx].split(' ')[idx1]
        		tagged_list.append({ val: tag_lines[idx].split(' ')[idx1] })
        		t_lables.append(tag_lines[idx].split(' ')[idx1])
        vec = DictVectorizer()
        
        pos_vectorized = vec.fit_transform(tagged_list).toarray()
        
        train_data = []
        train_labels = []
        test_data = []

        test_data = raw_input("--Enter the sentence to be tagged-- ").split(" ")

        for curr_class in classes:
            dirname = os.path.join(data_dir, curr_class)
            for fname in os.listdir(dirname):
                with open(os.path.join(dirname, fname), 'r') as f:
                    content = f.read()

                    if fname.startswith('train'):
                        train_data.append(content)
                        train_labels.append(curr_class)


        X_train, X_test, Y_train, Y_test = train_test_split(pos_vectorized, t_lables, test_size=0.10, train_size=0.90)
        # print type(test_data)


        vectorizer = TfidfVectorizer(min_df=0,
                                 max_df = 1,
                                 sublinear_tf=True,
                                 strip_accents='unicode',
                                 analyzer='word',
                                 use_idf=False,decode_error='ignore')

        train_vectors = vectorizer.fit_transform(train_data)
        #print train_vectors
        # print type(train_vectors)
        test_vectors = vectorizer.transform(test_data)
        # t_data = vec.transform(test_data)

        # Perform classification with SVM, kernel=rbf
        classifier_rbf = svm.LinearSVC()
        t0 = time.time()
        classifier_rbf.fit(X_train, Y_train)
        t1 = time.time()
        prediction_rbf = classifier_rbf.predict(test_vectors)
        t2 = time.time()
        time_rbf_train = t1-t0
        time_rbf_predict = t2-t1

        print("Results for SVC")
        print "prediction"
        print prediction_rbf

p = Predictor();
while True:
 p.predict()
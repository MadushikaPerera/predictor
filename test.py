# coding: utf-8
import sys
import os
import time

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.metrics import classification_report

class Predictor:
    def __init__(self):
        print "Predictor running ..."

    def predict(self):

        data_dir = "./text"
        classes = ['KPD', 'KRW', 'NIP', 'NMP', 'NMV', 'UPS']

        # Read the data
        train_data = []
        train_labels = []
        test_data = []
        stop_labels = []
        stop_data = []
        data = []

        test_data = raw_input("--Enter the sentence to be tagged-- ").split(" ")

        f = file("./text/train.txt").read()
        for word in f.split():
            # do something with word
            # print word
            data.append(word)

        for i in xrange(len(data)):
            if (i % 2) == 0:
                train_data.append(data[i])
            else:
                train_labels.append(data[i])



        vectorizer = TfidfVectorizer(min_df=0,
                                 max_df = 1,
                                 sublinear_tf=True,
                                 stop_words=stop_data,
                                 strip_accents='unicode',
                                 analyzer='word',
                                 use_idf=False,decode_error='ignore')

        train_vectors = vectorizer.fit_transform(train_data)
        #print train_vectors
        # print type(train_vectors)
        test_vectors = vectorizer.transform(test_data)
        print test_vectors
        # print test_vectors

        # Perform classification with SVM, kernel=rbf
        classifier_rbf = svm.LinearSVC()
        t0 = time.time()
        classifier_rbf.fit(train_vectors, train_labels)
        t1 = time.time()
        prediction_rbf = classifier_rbf.predict(test_vectors)
        t2 = time.time()
        time_rbf_train = t1-t0
        time_rbf_predict = t2-t1

        print("Results for SVC")
        print "prediction"
        print prediction_rbf
        print
        # print(classification_report(test_labels, prediction_rbf))
        # print("Results for SVC(kernel=linear)")
        # print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))
        # print(classification_report(test_labels, prediction_linear))
        # print("Results for LinearSVC()")
        # print("Training time: %fs; Prediction time: %fs" % (time_liblinear_train, time_liblinear_predict))
        # print(classification_report(test_labels, prediction_liblinear))

p = Predictor();
while True:
 p.predict()
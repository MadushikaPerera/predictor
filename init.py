# coding: utf-8
import sys
import os
import time

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.metrics import classification_report

class Predictor:
    def __init__(self):
        print "Predictor running ..."

    def predict(self):

        data_dir = "./txt"
        classes = ['positive', 'negative']

        # Read the data
        train_data = []
        train_labels = []
        test_data = []
        test_labels = []
        stop_labels = []
        stop_data = []

        for curr_class in classes:
            dirname = os.path.join(data_dir, curr_class)
            for fname in os.listdir(dirname):
                with open(os.path.join(dirname, fname), 'r') as f:
                    content = f.read()
                    if fname.startswith('test'):
                        test_data.append(content)
                        test_labels.append(curr_class)
                    elif fname.startswith('train'):
                        train_data.append(content)
                        train_labels.append(curr_class)
                    elif fname.startswith('stop'):
                        stop_data.append(content)
                        stop_labels.append('stop')

        vectorizer = TfidfVectorizer(min_df=0.2,
                                 max_df = 0.8,
                                 sublinear_tf=True,
                                 stop_words=stop_data,
                                 strip_accents='unicode',
                                 analyzer='word',
                                 use_idf=True,decode_error='ignore')

        train_vectors = vectorizer.fit_transform(train_data)
        # print "train vectors"
        # print type(train_vectors)
        print test_data
        test_vectors = vectorizer.transform(test_data)

        print test_vectors

        # print test_vectors

        # Perform classification with SVM, kernel=rbf
        classifier_rbf = svm.SVC()
        t0 = time.time()
        classifier_rbf.fit(train_vectors, train_labels)
        t1 = time.time()
        prediction_rbf = classifier_rbf.predict(test_vectors)
        t2 = time.time()
        time_rbf_train = t1-t0
        time_rbf_predict = t2-t1

        # Perform classification with SVM, kernel=linear
        # classifier_linear = svm.SVC(kernel='linear')
        # t0 = time.time()
        # classifier_linear.fit(train_vectors, train_labels)
        # t1 = time.time()
        # prediction_linear = classifier_linear.predict(test_vectors)
        # t2 = time.time()
        # time_linear_train = t1-t0
        # time_linear_predict = t2-t1

        # # Perform classification with SVM, kernel=linear
        # classifier_liblinear = svm.LinearSVC()
        # t0 = time.time()
        # classifier_liblinear.fit(train_vectors, train_labels)
        # t1 = time.time()
        # prediction_liblinear = classifier_liblinear.predict(test_vectors)
        # t2 = time.time()
        # time_liblinear_train = t1-t0
        # time_liblinear_predict = t2-t1

        # Print results in a nice table
        print("Results for SVC")
        # print("Training time: %fs; Prediction time: %fs" % (time_rbf_train, time_rbf_predict))
        print "prediction"
        print prediction_rbf
        # print(classification_report(test_labels, prediction_rbf))
        # print("Results for SVC(kernel=linear)")
        # print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))
        # print(classification_report(test_labels, prediction_linear))
        # print("Results for LinearSVC()")
        # print("Training time: %fs; Prediction time: %fs" % (time_liblinear_train, time_liblinear_predict))
        # print(classification_report(test_labels, prediction_liblinear))

p = Predictor();
p.predict()